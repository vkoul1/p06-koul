//
//  GameScene.swift
//  Runners
//
//  Created by Vikas Koul on 4/6/17.
//  Copyright © 2017 Vikas Koul. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
   // Generating variables
    let herospeed: CGFloat = 120.0
    let zombiespeed: CGFloat = 60.0
    let fireballspeed : CGFloat = 75.0
    
    var hero: SKSpriteNode?
    var zombie: [SKSpriteNode]=[]
    var lastpointtouched : CGPoint? = nil
    var goal: SKSpriteNode?
    var fireNode: SKSpriteNode?
    
    override func didMove(to view: SKView) {
        physicsWorld.contactDelegate = self
        //Setting up the hero
        hero = self.childNode(withName: "hero") as? SKSpriteNode
        self.listener = hero
        //Setting the zobmbies in the game
        for child in self.children{
            if child.name == "zombie"{
                if let child = child as? SKSpriteNode{
                    zombie.append(child);
                }
            }
        }
        goal = self.childNode(withName: "goal") as? SKSpriteNode
        updateCamera()
    }
    override func didSimulatePhysics() {
        if let _ = hero{
            updateHeroPosition();
            updateZombiePosition();
        }
    }
    
    func chkPosition(currentPosition: CGPoint,touchPosition:CGPoint) -> Bool{
        return abs(currentPosition.x - touchPosition.x) > hero!.frame.width/2 || abs(currentPosition.y - touchPosition.y) > hero!.frame.height/2
    }
    func updateHeroPosition(){
        if let touch = lastpointtouched{
            let currentpos = hero!.position
            if chkPosition(currentPosition: currentpos,touchPosition: touch){
                let movementAngle = atan2(currentpos.y - touch.y, currentpos.x - touch.x) + CGFloat(M_PI)
                let rotateAction = SKAction.rotate(toAngle: movementAngle + CGFloat(M_PI*0.5), duration: 0)
                hero!.run(rotateAction)
                let heroX = herospeed * cos(movementAngle)
                let heroY = herospeed * sin(movementAngle)
                
                let heroVelocity = CGVector(dx: heroX, dy: heroY)
                hero!.physicsBody!.velocity = heroVelocity
                updateCamera()
            }
            else{
                hero!.physicsBody!.isResting = true
            }
        }
    }

    func updateZombiePosition(){
        let targetHero = hero!.position
        for z in zombie{
            let currentPosition = z.position
            let movementAngle = atan2(currentPosition.y - targetHero.y, currentPosition.x - targetHero.x) + CGFloat(M_PI)
            let rotateAction = SKAction.rotate(toAngle: movementAngle + CGFloat(M_PI*0.5), duration: 0)
            z.run(rotateAction)
            let zomX = zombiespeed * cos(movementAngle)
            let zomY = zombiespeed * sin(movementAngle)
            
            let zombieVelocity = CGVector(dx: zomX, dy: zomY)
            z.physicsBody!.velocity = zombieVelocity

        }
    }
    
    func fireball() {
        //print("in fireball func");
        let currentpos = hero!.position
        let touch = lastpointtouched
        let movementAngle = atan2(currentpos.y - (touch?.y)!, currentpos.x - (touch?.x)!) + CGFloat(M_PI)
        let rotateAction = SKAction.rotate(toAngle: movementAngle + CGFloat(M_PI*0.5), duration: 0)
        hero!.run(rotateAction)
        //let fireX = fireballspeed * cos(movementAngle)
        let fireY = fireballspeed * sin(movementAngle)
        fireNode = SKSpriteNode(imageNamed: "fireball.png")

        fireNode?.position.y += fireY
        //fireNode?.position.x += fireX
        fireNode?.setScale(0.3)
        fireNode?.physicsBody = SKPhysicsBody(circleOfRadius: (fireNode?.size.width)! / 2)
        fireNode?.physicsBody?.isDynamic = true
        
        fireNode?.physicsBody?.categoryBitMask = 3
        fireNode?.physicsBody?.contactTestBitMask = 2
        fireNode?.physicsBody?.usesPreciseCollisionDetection = true
        
        self.addChild(fireNode!)

        var actionArray = [SKAction]()
        //print("X in array is ")
        //print(hero!.position.x)
        //print("Y in array is ");
        //print(self.frame.size.height + 10)
        actionArray.append(SKAction.move(to: CGPoint(x: (hero?.position.x)!, y: fireY), duration: 0.5))
        actionArray.append(SKAction.removeFromParent())
        fireNode?.run(SKAction.sequence(actionArray))
        
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        handleTouches(touches: touches)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        handleTouches(touches: touches)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        handleTouches(touches: touches)
    }
    
    private func handleTouches(touches: Set<UITouch>){
        for touch in touches{
            let touchLoc = touch.location(in: self)
            lastpointtouched = touchLoc
          //  fireball()
        }
    }
    func updateCamera(){
        if let cam = camera {
            cam.position = CGPoint(x: hero!.position.x, y: hero!.position.y)
        }
    }
    func didBegin(_ contact: SKPhysicsContact) {
        //print("in didBegin method")
        var firstOne : SKPhysicsBody
        var secondOne : SKPhysicsBody
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask{
            //print("in if")
            firstOne = contact.bodyA
            secondOne = contact.bodyB
        }
        else{
            //print("in else")
            firstOne = contact.bodyB
            secondOne = contact.bodyA
        }
        if firstOne.categoryBitMask == fireNode?.physicsBody?.categoryBitMask && secondOne.categoryBitMask == zombie[0].physicsBody?.categoryBitMask{
            //zombie[0].removeFromParent()
            print("fireNode hits zombie")
        }
        else if firstOne.categoryBitMask == hero?.physicsBody?.categoryBitMask && secondOne.categoryBitMask == zombie[0].physicsBody?.categoryBitMask{
          //  print("in if with gameover false")
            gameOver(winCheck: false)
        }
        else if firstOne.categoryBitMask == hero?.physicsBody?.categoryBitMask && secondOne.categoryBitMask == goal?.physicsBody?.categoryBitMask{
      //      print("in if with gameover true")
            gameOver(winCheck: true)
        }
    
    }
    
    func gameOver(winCheck: Bool)
    {
        hero?.isHidden = true
        for z in zombie{
            z.isHidden = true
        }
        self.removeAllChildren();
        if winCheck
        {
            //self.backgroundColor = SKColor(red: 0, green:0, blue:0, alpha: 1)
            let label = SKLabelNode(text: "You Won")
            label.fontName = "AvenirNext-Bold"
            label.fontSize = 60
            label.fontColor = UIColor.white
            label.position = CGPoint(x: self.frame.midX, y : self.frame.midY)
            self.addChild(label)
        }
        else{
            //self.backgroundColor = SKColor(red: 0, green:0, blue:0, alpha: 1)
            let label = SKLabelNode(text: "Game Over")
            label.fontName = "AvenirNext-Bold"
            label.fontSize = 60
            label.fontColor = UIColor.white
            label.position = CGPoint(x: self.frame.midX, y : self.frame.midY)
            self.addChild(label)
            let delayInRestart = 5.0
            let transition = SKTransition.flipVertical(withDuration: 1.0)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+delayInRestart){
                let gameScene = GameScene(fileNamed: "GameScene")
                let skView = self.view as SKView!
                gameScene?.scaleMode = .aspectFill
                skView?.presentScene(gameScene!, transition: transition)
            }
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}


